FROM ubuntu:18.04 as base

MAINTAINER Yann Richet <yann.richet@irsn.fr>


RUN apt-get update
RUN apt-get install -y apt-utils


## Install VNC & deps
RUN apt-get install -y locales
RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

RUN apt-get install -y x11vnc xvfb xdotool xcompmgr x11-xserver-utils
RUN mkdir ~/.vnc
RUN x11vnc -storepasswd snitch ~/.vnc/passwd
# Overload the password with standard one from snicker (dynamically when 'docker run')
RUN rm ~/.vnc/passwd
RUN ln -s /snicker/passwd ~/.vnc/passwd
# Add vnc2flv
RUN apt-get install -y curl build-essential python-dev
RUN curl https://bootstrap.pypa.io/get-pip.py --output get-pip.py
RUN python2 get-pip.py
RUN pip2 install vnc2flv
RUN apt-get install -y ffmpeg

# OpenBox needs python2
RUN apt-get install -y openbox python


## Install snitch + deps
RUN apt-get install -y software-properties-common
RUN rm -rf /var/lib/apt/lists/*
RUN add-apt-repository -y ppa:alex-p/tesseract-ocr
RUN apt-get update
RUN apt-get install -y tesseract-ocr

RUN apt-get install -y python3-xlib python3-tk python3-dev scrot python3-pip python3-git python3-pyqt5
# bug in standard version of PyQt5, so overload with version 5.14 (but keep previous install for dependencies)
RUN pip3 install pyqt5==5.14
# standard pynput not working inside x11vnc: https://github.com/moses-palmer/pynput@fixup-xorg-fake-events
RUN pip3 install git+https://github.com/yannrichet/pynput
RUN pip3 install snitch-ci


## Setup working directory & start snitch

# /WORKDIR: will be overloaded if 'docker run ... -v ...:/WORKDIR'
# Map /WORKDIR to /builds/user/project if any (standard path for gitlab-ci). We assume there is just one if /builds exists.
RUN if [ -d /builds ]; then wd=$(readlink -f `ls -d /builds/*/*`); ln -s $wd /WORKDIR; fi
# Create empty /WORKDIR at least. Required.
RUN if [ ! -d /WORKDIR ]; then mkdir /WORKDIR; fi
WORKDIR /WORKDIR

# Get latest docker_snitch.sh script, will be overloaded if 'docker run ... -v ...:/snicker'
RUN if [ ! -d /snicker ]; then git clone https://gitlab.com/IRSN/Snicker /snicker; fi
# Will append arguments (if any) to "docker run ... snitch ARG1 ARG2" as arguments to run_snitch.sh"
ENTRYPOINT ["/snicker/run_snitch.sh"] 
