#!/bin/bash
# Run Snitch with fixed graphical context (screen size, xterm, ...)


export DISPLAY=":0"
XTERM_OPTS="-display $DISPLAY -geometry 100x50+850+10 -en -utf8"
WORKDIR="/WORKDIR"
LOGDIR=$WORKDIR/log
mkdir -p $LOGDIR
DUMPDIR=$WORKDIR/dump
mkdir -p $DUMPDIR

f=""
dump="0"
timeout="600"
export DATE=$(date +"%H-%M_%d-%m-%y")

usage() {
cat <<EOFusage
usage : $0 [-h]
           [-f <snitch record.json file to play> ()] 
               [-d] (finally dump screenshot in png files)
EOFusage
}
while getopts  "f:hd" flag; do
 case $flag in
    f) f=$OPTARG;;
    d) dump="1";;
    h) usage
       exit 0;;
 esac
done
shift $(( $OPTIND-1 ))

if [ $# -eq 0 ]
  then
    # No arguments supplied -> default screen resolution
    export SCREEN="1600x900x24"
  else
    export SCREEN=$1
fi

echo "ARGS: $*" > $LOGDIR/run_snitch.log 2>&1
echo "f: $f" >> $LOGDIR/run_snitch.log 2>&1
echo "screen: $SCREEN" >> $LOGDIR/run_snitch.log 2>&1
echo "dump: $dump" >> $LOGDIR/run_snitch.log 2>&1
case_name=${f/.json} 
export case_name=${case_name/"/"} 

setxkbmap en

mkdir -p $HOME/.local/share

Xvfb $DISPLAY -screen 0 $SCREEN > $LOGDIR/Xvfb.log 2>&1 &
xhost +
# Even with this xhost+, python-Xlib requires an ~/.Xauthority file to be available...
export XAUTHORITY=$HOME/.Xauthority
touch $XAUTHORITY
xauth add `hostname`/unix$DISPLAY . ABCDEF >> $LOGDIR/auth.log 2>&1
xauth nlist $DISPLAY | sed -e 's/^..../ffff/' | xauth nmerge - >> $LOGDIR/auth.log 2>&1
more $HOME/.Xauthority >> $LOGDIR/auth.log 2>&1

x11vnc -shared -display $DISPLAY -forever -usepw -noxdamage -noxrecord -nowf -noscr -auth $XAUTHORITY > $LOGDIR/x11vnc.log 2>&1 &

# Just keep one desktop in openbox
mkdir $HOME/.config
sed "s/number>4/number>1/g" /etc/xdg/openbox/rc.xml > $HOME/.config/rc.xml 
openbox-session > $LOGDIR/openbox.log 2>&1 &
sleep 2
xcompmgr -d $DISPLAY > $LOGDIR/xcompmgr.log 2>&1 &

# Setup defaults for Snitch (especially minimize_on_record)
echo '{ "minimize_on_record": true, "replay_delay": 0.2, "diff_threshold": 98, "history": { "max_size": 10, "last_location": "/WORKDIR", "last_files": [] } }' > $HOME/.snitch.json

# Optional before_snitch.sh execution. May include dependencies install using apt-get inside docker ubuntu image
if [ -f $WORKDIR/before_snitch.sh ]; then
  sh $WORKDIR/before_snitch.sh > $LOGDIR/before_snitch.log 2>&1
fi

cd /tmp

if [ "$dump" == "1" ]; then
   flvrec.py -P /snicker/passwd.txt -o $case_name.$DATE.flv -d localhost$DISPLAY > $LOGDIR/flvrec.log 2>&1 &
   PID_FLVREC=$!
fi

cd /WORKDIR

if [ ! "$f" == "" ]; then # play mode ('snitch -f record.json')
  xterm $XTERM_OPTS > $LOGDIR/xterm.log 2>&1 &

  sleep 2
  # Copy f in WORKDIR, to be removed when snitch support expliit output dir
  cp $f $WORKDIR/.
  result=`sed "s/json/result.json/g" <<< $(basename "$f")`
  cd $WORKDIR
  snitch -v DEBUG -f $(basename "$f") > $LOGDIR/snitch.log 2>&1
  EXIT=$?
  echo $EXIT > $LOGDIR/snitch.exit
  
  if [ "$dump" == "1" ]; then
    snitch-dump-images $result >> $LOGDIR/run_snitch.log 2>&1
  fi
else #record mode, because no target json file. So start snitch GUI
  snitch -v DEBUG > $LOGDIR/snitch.log 2>&1 &
  # First thing to do: start recording
  SNITCH_WINDOW_ID=""
  while [ -z "$SNITCH_WINDOW_ID" ]
  do
    sleep 1
    SNITCH_WINDOW_ID=`xdotool search --onlyvisible snitch`
  done
  eval $(xdotool getmouselocation --shell) #Store location of mouse in $X and $Y
  
  xdotool windowmove $SNITCH_WINDOW_ID 0 0
  xdotool mousemove 60 50
  xdotool click 1
  xdotool mousemove $X $Y
  
  xterm $XTERM_OPTS > $LOGDIR/xterm.log 2>&1
  EXIT=$?
  echo $EXIT > $LOGDIR/xterm.exit
fi

# Optional after_snitch.sh execution. May include dependencies install using apt-get inside docker ubuntu image
if [ -f $WORKDIR/after_snitch.sh ]; then
  sh $WORKDIR/after_snitch.sh > $LOGDIR/after_snitch.log 2>&1
fi

# dump pictures and videos
cd /tmp
if [ "$dump" == "1" ]; then
  kill -2 $PID_FLVREC
  for out in `ls *.flv`; do 
    ffmpeg -i $out -c:v libx264 -crf 19 $out.mp4 >> $LOGDIR/flvrec.log 2>&1
    chmod -R a=u $out $out.mp4
    mv $out $out.mp4 $DUMPDIR
  chmod -R a=u *.png
  mv $WORKDIR/*.png $DUMPDIR
  done
fi

exit $EXIT
