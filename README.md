# Snicker (Snitch-CI inside Docker)

This project is a tight integration of Snitch-CI and Docker, for Debian/Ubuntu.
It allows to use a fixed display for recording and replaying Snitch macros, thus solving problems with display settings.

## Install

* just download Snicker somewhere: `git clone https://gitlab.com/IRSN/Snicker`
* and install dependencies: `sudo apt-get install docker.io procps xtightvncviewer`

## Usage

* recording a new macro: `[sudo] /path/to/snicker/snicker.sh`
* recording a new macro, mounting target application path: `[sudo] /path/to/snicker/snicker.sh -a /path/to/app`
* playing a macro: `[sudo] /path/to/snicker/snicker.sh -f /path/to/macro`
* playing a macro, with target application path (assumed portable): `[sudo] /path/to/snicker/snicker.sh -f /path/to/macro -a /path/to/app`
* playing a macro, with display and target application path (assumed portable): `[sudo] /path/to/snicker/snicker.sh -f /path/to/macro -v -a /path/to/app`

You should also need to modify the Dockerfile for dependencies of your app, but it is highly recommended to provide a "portable" installation of the target application to test.

## GitLab integration

See the projet [Snicker-HelloWorld](https://gitlab.com/IRSN/Snicker-HelloWorld) for an example of using Snitch & Snicker in GitLaB CI environment.
