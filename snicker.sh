#!/bin/bash
# Run Snitch inside docker with virtual screen

SNICKER_PATH=$(readlink -f `dirname $0`)
WORKDIR=$PWD
LOGDIR=$WORKDIR"/log"
mkdir -p $LOGDIR

# Set default permissions of $LOGDIR
chmod g+s $LOGDIR
setfacl -d -m g::rw $LOGDIR
setfacl -d -m o::rw $LOGDIR

if [ "`command -v docker`" == "" ]; then
    # Assume debian/ubuntu
    if [ $(dpkg-query -W -f='${Status}' docker 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
        echo "  'docker' missing. Install:"
        apt-get install -y docker.io;
        # curl -sSL https://get.docker.com/ | sh
    fi
fi
if [ "`command -v pkill`" == "" ]; then
    # Assume debian/ubuntu
    if [ $(dpkg-query -W -f='${Status}' procps 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
        echo "  'pkill' missing. Install:"
        apt-get install -y procps;
    fi
fi
install_xtightvncviewer() {
    if [ "`command -v xtightvncviewer`" == "" ]; then
        # Assume debian/ubuntu
        if [ $(dpkg-query -W -f='${Status}' xtightvncviewer 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
            echo "  'xtightvncviewer' missing. Install:"
            apt-get install -y xtightvncviewer;
        fi
    fi
}


if [ "`docker info | grep Version | wc -l`" -lt 1 ]; then 
    echo "Starting docker daemon..."
    # Assume debian/ubuntu
    service docker start
    #dockerd -D -H tcp://0.0.0.0:2376 -H fd:// -H unix:///var/run/docker.sock > $LOGDIR/dockerd.log 2>&1 &
    # may also use https://github.com/billyteves/ubuntu-dind/blob/master/wrapdocker instead: bash $SNICKER_PATH/wrapdocker
fi

if [ `docker images | grep "irsn/snicker" | wc -l` == "0" ]; then
    #  [Create snitch docker image using:  "sudo docker build -t "irsn/snicker" ."]
    echo "Building docker image 'snicker'..."
    if [ ! -f $SNICKER_PATH/Dockerfile ]; then
        echo "Missing Dockerfile, exiting !"
        exit 1
    fi
    cd $SNICKER_PATH
    docker build -t "irsn/snicker" .
    cd $WORKDIR
fi

f=""
app="."
view="0"
dump="0"
timeout="600"
screen="1600x900x24"

usage() {
cat <<EOFusage
usage : $0 [-h]
           [-a <application dir, mounted in /app> (.)]
           [-f <snitch record.json file to play> ()] 
           [-v] (view playing)
           [-t <timeout for running case> (60)]
           [-s <screen resolution WxHxD> (1600x900x24)]
           [-d] (finally dump screenshot & movie files)
EOFusage
}
while getopts  "f:a:vhts:d" flag; do
 case $flag in
    f) f=$OPTARG;;
    a) app=$OPTARG;;
    v) view="1";;
    t) timeout=$OPTARG;;
    s) screen=$OPTARG;;
    d) dump="1";;
    h) usage
       exit 0;;
 esac
done
shift $(( $OPTIND-1 ))

PID=$$
teardown() {
    echo "Received exit signal"

    # Once vncviewer ends, stop docker vm. TODO: Replace by docker stop
    STOP_PID=""
    if [ -f $LOGDIR/snicker.id ]; then
        echo "Stop dockers:"
        while read line
        do
            echo "  "$line
            docker stop $line &
            STOP_PID=$!
        done < $LOGDIR/snicker.id
        rm $LOGDIR/snicker.id
    fi
    
    pkill -P -3 $PID
    pkill -P -9 $PID

    EXIT="666"
    if [ -f $LOGDIR/snitch.exit ]; then
        echo "Get exit status"
        while read line
        do
            EXIT=`cat $LOGDIR/snitch.exit`
        done < $LOGDIR/snitch.exit
        rm $LOGDIR/snitch.exit
    else
        EXIT="-1"
    fi
    echo "Exit: "$EXIT
    
    if [ ! $STOP_PID == "" ]; then
        wait $STOP_PID
    fi
    exit $EXIT
}
trap teardown INT

if [ "$f" == "" ]; then
    echo "Start docker with:"
    echo "  * "$(readlink -f $app)" -> /`basename $(readlink -f $app)`"
    echo "  * "$(readlink -f ".")" -> /WORKDIR"
    #echo "  * "$SNICKER_PATH" -> /snicker"
    if [ "$dump" == "1" ]; then
        docker run -d --rm -ti \
                      -p 5900:5900 \
                      -v $(readlink -f $app):"/`basename $(readlink -f $app)`" \
                      -v $(readlink -f "."):/WORKDIR \
                      -v $SNICKER_PATH:/snicker \
                      "irsn/snicker" -d $screen >> $LOGDIR/snicker.id
    else
        docker run -d --rm -ti \
                      -p 5900:5900 \
                      -v $(readlink -f $app):"/`basename $(readlink -f $app)`" \
                      -v $(readlink -f "."):/WORKDIR \
                      -v $SNICKER_PATH:/snicker \
                      "irsn/snicker" $screen >> $LOGDIR/snicker.id
    fi
    sleep 3
    echo "Start VNC client"
    install_xtightvncviewer
    xtightvncviewer localhost:5900 -passwd $SNICKER_PATH/passwd > $LOGDIR/snitch-vncviewer.log 2>&1
else
    echo "Start docker with:"
    echo "  * "$(readlink -f $app)" -> /`basename $(readlink -f $app)`"
    echo "  * "$(readlink -f ".")" -> /WORKDIR"
    #echo "  * "$SNICKER_PATH" -> /snicker"
    echo "  * "$(readlink -f "$f")" -> /"$(basename "$f")
    if [ "$dump" == "1" ]; then
        echo "  * dump-images"
        docker run -d --rm -ti \
                      -p 5900:5900 \
                      -v $(readlink -f $app):"/`basename $(readlink -f $app)`" \
                      -v $(readlink -f "."):/WORKDIR \
                      -v $(readlink -f "$f"):/$(basename "$f") \
                      -v $SNICKER_PATH:/snicker \
                      "irsn/snicker" -f /$(basename "$f") -d $screen >> $LOGDIR/snicker.id
    else
        docker run -d --rm -ti \
                      -p 5900:5900 \
                      -v $(readlink -f $app):"/`basename $(readlink -f $app)`" \
                      -v $(readlink -f "."):/WORKDIR \
                      -v $(readlink -f "$f"):/$(basename "$f") \
                      -v $SNICKER_PATH:/snicker \
                      "irsn/snicker" -f /$(basename "$f") $screen >> $LOGDIR/snicker.id
    fi
    if [ $view == "1" ]; then
        sleep 5
        echo "Start VNC viewer"
        install_xtightvncviewer
        xtightvncviewer localhost:5900 -passwd $SNICKER_PATH/passwd -viewonly > $LOGDIR/snitch-vncviewer.log 2>&1
    else
        DID=`tail -1 $LOGDIR/snicker.id | cut -c 1-8`
        echo -n "Waiting docker: "$DID
        while [ ! `docker ps | grep $DID | wc -l` == "0" ]; do
            ((i++)) && ((i>=$timeout)) && echo " Timeout !" && break
            echo -n "."
            sleep 1
        done
        echo ""
    fi
fi

teardown
